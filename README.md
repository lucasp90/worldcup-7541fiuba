# README #

### ¿Para qué es este repositorio? ###

* He creado este repositorio para guardar un trabajo práctico realizado en la materia Algoritmos y Programación II, correspondiente a la Facultad de Ingeniería de la Universidad de Buenos Aires. El mismo consiste en un backend que administra información correspondiente a un torneo de fútbol de eliminación directa. Se pueden simular resultados, consultar goleadores, equipos y resultados.
* La versión corresponde a la entrega del trabajo durante la cursada.

### Setting ###

* Recomiendo compilación por consola con el uso del Makefile provisto en el repositorio.
* El programa recibe por parámetro un archivo de texto que contiene los equipos y jugadores del torneo. El formato está especificado en el enunciado (tp2-2013-2.pdf)
* Para correr los tests provistos por la cátedra, leer el enunciado


### Who do I talk to? ###

* Para cualquier consulta, enviar inbox a https://bitbucket.org/lucasp90